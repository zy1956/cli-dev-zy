'use strict';

const fs = require('fs')
const path = require('path')
const inquirer = require('inquirer')
const fse = require('fs-extra')
const glob = require('glob')
const ejs = require('ejs')
const semver = require('semver')
const userHome = require('user-home')
const Command = require('@cli-dev-zy/command')
const log = require('@cli-dev-zy/log');
const Package = require('@cli-dev-zy/package')
const { spinnerStart, sleep, execAsync } = require('@cli-dev-zy/utils')
const getProjectTemplate = require('./getProjectTemplate')

const TYPE_PROJECT = 'project'
const TYPE_COMPONENT = 'component'
const TEMPLATE_TYPE_NORMAL = 'normal'
const TEMPLATE_TYPE_CUSTOM = 'custom'

const WHITE_COMMAND = ['npm', 'cnpm', 'yarn']
class InitCommand extends Command {
    init() {
        this.projectName = this._argv[0] || ''
        this.force = !!this._cmd.force
        log.verbose('InitCommand-projectName', this.projectName);
        log.verbose('InitCommand-force', this.force);
    }
    async exec() {
        try {
            // 1. 准备阶段
            const projectInfo = await this.prepare()
            log.verbose('projectInfo', projectInfo);
            if (!projectInfo) return
            this.projectInfo = projectInfo
            // 2. 下载模板
            await this.downloadTemplate()
            // 4. 安装模板  
            await this.installTemplate()
        } catch (e) {
            log.error(e.message)
            if (process.env.lOG_LEVEL === 'verbose') {
                console.log(e)
            }
        }
    }
    async installTemplate() {
        log.verbose('templateInfo', this.templateInfo)
        if (this.templateInfo) {
            this.templateInfo.type = this.templateInfo.type || TEMPLATE_TYPE_NORMAL
            if (this.templateInfo.type === TEMPLATE_TYPE_NORMAL) {
                // 标准安装
                await this.installNormalTemplate()
            } else if (this.templateInfo.type === TEMPLATE_TYPE_CUSTOM) {
                // 自定义安装
                await this.installCustomlTemplate()
            } else {
                throw new Error('无法识别项目模板类型')

            }
        } else {
            throw new Error('项目模板信息不存在')
        }
    }
    checkCommand(cmd) {
        if (WHITE_COMMAND.includes(cmd)) {
            return cmd
        } else {
            return null
        }
    }
    async execCommand(command, errMsg = '命令执行失败') {
        let ret;
        if (command) {
            const cmdArrat = command.split(' ')
            const cmd = this.checkCommand(cmdArrat[0])
            if (!cmd) {
                throw new Error('命令不存在!', command)
            }
            const args = cmdArrat.slice(1)
            ret = await execAsync(cmd, args, {
                cwd: process.cwd(),
                stdio: 'inherit'
            })
        }
        if (ret !== 0) {
            throw new Error(errMsg)
        }
        return ret
    }

    async ejsRender(options) {
        const dir = process.cwd()
        return new Promise((resolve, reject) => {
            glob('**', {
                cwd: dir,
                ignore: options.ignore,
                nodir: true,
            }, (err, files) => {
                if (err) {
                    reject(err)
                }
                Promise.all(files.map(file => {
                    const filePath = path.join(dir, file)
                    return new Promise((resolve1, reject1) => {
                        ejs.renderFile(filePath, this.projectInfo, {}, (err, result) => {
                            // console.log(err, result);
                            if (err) {
                                reject1(err)
                            } else {
                                fse.writeFileSync(filePath, result)
                                resolve1(result)
                            }
                        })
                    })
                })).then(() => {
                    resolve()
                }).catch(err => {
                    reject(err)
                })
            })
        })
    }

    async installNormalTemplate() {
        log.verbose('templateNpm', this.templateNpm)
        // 拷贝模板代码至当前目录
        const spinner = spinnerStart('正在安装模板')
        await sleep()
        try {
            const templatePath = path.resolve(this.templateNpm.cacheFilePath, 'template')
            const targetPath = process.cwd()
            fse.ensureDirSync(templatePath)
            fse.ensureDirSync(targetPath)
            fse.copySync(templatePath, targetPath)
        } catch (e) {
            throw e
        } finally {
            spinner.stop(true)
            log.success('模板安装成功')
        }
        // 动态配置ignore
        const templateIgnore = this.templateInfo.ignore || []
        const ignore = ['**/node_modules/**', ...templateIgnore]
        await this.ejsRender({ ignore })
        const { installCommand, startCommand } = this.templateInfo
        // 依赖安装
        await this.execCommand(installCommand, '依赖安装过程中失败!')
        // 启动命令执行
        await this.execCommand(startCommand, '执行启动命令失败!')
    }

    async installCustomlTemplate() {
        // 查询自定义模板的入口文件
        if (await this.templateNpm.exists()) {
            const rootFile = this.templateNpm.getRootFilePath();
            if (fs.existsSync(rootFile)) {
                log.notice('开始执行自定义模板');
                const templatePath = path.resolve(this.templateNpm.cacheFilePath, 'template');
                const options = {
                    templateInfo: this.templateInfo,
                    projectInfo: this.projectInfo,
                    sourcePath: templatePath,
                    targetPath: process.cwd(),
                };
                const code = `require('${rootFile}')(${JSON.stringify(options)})`;
                log.verbose('code', code);
                await execAsync('node', ['-e', code], { stdio: 'inherit', cwd: process.cwd() });
                log.success('自定义模板安装成功');
            } else {
                throw new Error('自定义模板入口文件不存在！');
            }
        }
    }

    /**
     * 1. 通过项目模板API获取项目模板信息
     * 1.1 通过egg.js搭建一套后端系统
     * 1.2 通过npm存储项目模板（vue-cli/vue-element-admin）
     * 1.3 将项目模板信息存储到mongodb数据库中
     * 1.4 通过egg.js获取mongodb中的数据并且通过API返回
     */
    async downloadTemplate() {
        // console.log(this.projectInfo, this.template);
        const { projectTemplate } = this.projectInfo
        const templateInfo = this.template.find(item => item.npmName === projectTemplate)
        const targetPath = path.resolve(userHome, '.cli-dev-zy', 'template')
        const storeDir = path.resolve(userHome, '.cli-dev-zy', 'template', 'node_modules')
        const { npmName, version } = templateInfo
        this.templateInfo = templateInfo
        const templateNpm = new Package({
            targetPath,
            storeDir,
            packageName: npmName,
            packageVersion: version
        })
        if (!await templateNpm.exists()) {
            const spinner = spinnerStart('正在下载安装模板...')
            await sleep()
            try {
                await templateNpm.install()
            } catch (e) {
                throw e
            } finally {
                spinner.stop(true)
                if (await templateNpm.exists()) {
                    log.success('下载模板成功')
                    this.templateNpm = templateNpm
                }
            }
        } else {
            const spinner = spinnerStart('正在下载更新模板...')
            await sleep()
            try {
                await templateNpm.update()
            } catch (e) {
                throw e
            } finally {
                spinner.stop(true)
                if (await templateNpm.exists()) {
                    log.success('更新模板成功')
                    this.templateNpm = templateNpm
                }
            }
        }
    }

    async prepare() {
        // 0. 判断项目模板是否存在
        const template = await getProjectTemplate()
        // console.log(template);
        if (!template || template.length === 0) {
            throw new Error('项目模板不存在')
        }
        this.template = template
        // 1. 判断当前目录是否为空
        const localPath = process.cwd() // 当前工作目录
        if (!this.isDirEmpty(localPath)) {
            let ifContinue = false
            if (!this.force) {
                // 询问是否继续创建
                ifContinue = (await inquirer.prompt({
                    type: 'confirm',
                    name: 'ifContinue',
                    message: '当前文件夹不为空，是否继续创建项目？'
                })).ifContinue
                console.log(ifContinue);
                if (!ifContinue) {
                    console.log('停止创建');
                    return
                }
            }
            // 2. 是否启动强制更新
            if (ifContinue || this.force) {
                // 给用户做二次确认
                const { confirmDelete } = await inquirer.prompt({
                    type: 'confirm',
                    name: 'confirmDelete',
                    message: '是否确认清空当前目录下的文件？'
                })
                if (confirmDelete) {
                    // 清空当前目录
                    fse.emptyDirSync(localPath)
                }
            }
        }
        // return 项目的基本信息（object）
        return await this.getProjectInfo()
    }

    async getProjectInfo() {
        // 项目名称是否合法
        function isValidName(v) {
            // 1. 输入的首字符必须为英文字母
            // 2. 尾字符必须为英文或数字，不能为字符
            // 3. 字符允许"-_"
            // 合法: a, a-b, a_b, a-b-c, a-b1-c1,a_b1_c1a1,a1,a1-b1-c1, a1_b1_c1
            // 不合法: 1,a_,a-.a_1,a-1
            const reg = /^[a-zA-Z]+([-][a-zA-Z][a-zA-Z0-9]*|[_][a-zA-Z][a-zA-Z0-9]*|[a-zA-Z0-9])*$/
            // console.log(reg.test(1));
            return reg.test(v)
        }
        let projectInfo = {}
        let isProjectNameValid = false
        if (isValidName(this.projectName)) {
            isProjectNameValid = true
            projectInfo.projectName = this.projectName
        }
        // 1. 选择创建项目或组件
        const { type } = await inquirer.prompt({
            type: 'list',
            name: 'type',
            message: '请选择初始化类型',
            default: TYPE_PROJECT,
            choices: [{
                name: '项目',
                value: TYPE_PROJECT
            }, {
                name: '组件',
                value: TYPE_COMPONENT
            }]
        })
        log.verbose('type', type)
        // 过滤显示项目，组件对应的模板
        this.template = this.template.filter(temp => temp.tag.includes(type))
        const title = type === TYPE_PROJECT ? '项目' : '组件'
        const projectNamePrompt = {
            type: 'input',
            name: 'projectName',
            message: `请输入${title}的名称`,
            default: '',
            validate: function (v) {
                const done = this.async();
                setTimeout(function () {
                    if (!isValidName(v)) {
                        done(`请输入合法的${title}名称`);
                        return;
                    }
                    done(null, true);
                }, 0);
            },
            filter: function (v) {
                return v
            }
        }
        const projectPrompt = [];
        // 如果命令上没有输入项目名称,添加输入名称的Prompt
        if (!isProjectNameValid) {
            projectPrompt.push(projectNamePrompt)
        }
        projectPrompt.push({
            type: 'input',
            name: 'projectVersion',
            message: `请输入${title}版本号`,
            default: '1.0.0',
            validate: function (v) {
                const done = this.async();
                setTimeout(function () {
                    if (!(!!semver.valid(v))) {
                        done(`请输入合法的${title}版本号`);
                        return;
                    }
                    done(null, true);
                }, 0);
            },
            filter: function (v) {
                if (semver.valid(v)) {
                    return semver.valid(v)
                } else {
                    return v
                }
            }
        }, {
            type: 'list',
            name: 'projectTemplate',
            message: `请选择${title}模板`,
            choices: this.createTemplateChoice()
        })
        // 2. 获取项目的基本信息
        if (type === TYPE_PROJECT) {
            const project = await inquirer.prompt(projectPrompt)
            projectInfo = {
                ...projectInfo,
                type,
                ...project
            }
        } else if (type === TYPE_COMPONENT) {
            // 获取组件的基本信息
            const descriptionPrompt = {
                type: 'input',
                name: 'componentDescription',
                message: `请输入${title}描述信息`,
                default: '',
                validate: function (v) {
                    const done = this.async();
                    setTimeout(function () {
                        if (!v) {
                            done(`请输入${title}描述信息`);
                            return;
                        }
                        done(null, true);
                    }, 0);
                }
            }
            projectPrompt.push(descriptionPrompt)
            const component = await inquirer.prompt(projectPrompt)
            projectInfo = {
                ...projectInfo,
                type,
                ...component
            }
        }
        // 生成className69+35888
        // AbcEfg => abc-efg
        if (projectInfo.projectName) {
            projectInfo.name = projectInfo.projectName
            projectInfo.className = require('kebab-case')(projectInfo.projectName).replace(/^-/, '')
        }
        if (projectInfo.projectVersion) {
            projectInfo.version = projectInfo.projectVersion
        }
        if (projectInfo.componentDescription) {
            projectInfo.description = projectInfo.componentDescription
        }
        return projectInfo
    }

    // 判断目录是否为空
    isDirEmpty(localPath) {
        let fileList = fs.readdirSync(localPath)
        fileList = fileList.filter(file => (
            !file.startsWith('.') && ['node_modules'].indexOf(file) < 0
        ))
        return !fileList || fileList.length <= 0
    }

    createTemplateChoice() {
        return this.template.map(item => ({
            value: item.npmName,
            name: item.name
        }))
    }
}

function init(argv) {
    // console.log('init', projectName, options, command.opts(), process.env.CLI_TARGET_PATH);
    return new InitCommand(argv)
}

module.exports = init
module.exports.InitCommand = InitCommand;