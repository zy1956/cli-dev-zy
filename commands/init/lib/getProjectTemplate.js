const request = require('@cli-dev-zy/request')

module.exports = function () {
  return request({
    url: '/project/template',
  })
}