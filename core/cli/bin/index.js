#! /usr/bin/env node

const importLocal = require('import-local');
const log = require('npmlog')

if (importLocal(__filename)) {
    log.info('cli', '正在使用 cli-dev-zy 本地版本')
} else {
    // 查找当前目录，相当于require("./index.js") 并执行
    require('../lib')(process.argv.slice(2))
}