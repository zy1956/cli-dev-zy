'use strict';

module.exports = core;

const path = require('path')
const { Command } = require('commander')
const dedent = require('dedent')
const semver = require('semver')
const colors = require('colors/safe')
const chalk = require('chalk')
const pathExists = require('path-exists').sync // 判读目录是否存在
const userHome = require('user-home') // 获取当前用户主目录
const log = require('@cli-dev-zy/log')
const exec = require('@cli-dev-zy/exec')

const pkg = require('../package.json')
const constant = require('./const')

let args
const program = new Command()
const opts = program.opts()

async function core() {
    try {
        await prepare()
        registerCommand()
    } catch (e) {
        log.error(e.message)
        if (opts.debug) {
            console.log(e)
        }
    }
}

function registerCommand() {
    program
        .name(Object.keys(pkg.bin)[0])
        .usage('<command> [options]')
        .version(pkg.version)
        .option('-d, --debug', '是否开启调试模式', false)
        .option('-tp, --targetPath <targetPath>', '是否指定本地调试文件路径', '');

    program
        .command('init [projectName]')
        .option('-f --force', '是否强制初始化项目')
        .action(exec);

    // 监听debug
    program.on('option:debug', function () {
        if (opts.debug) {
            process.env.LOG_LEVEL = 'verbose'
        } else {
            process.env.LOG_LEVEL = 'info'
        }
        log.level = process.env.LOG_LEVEL
        log.verbose('debug', 'test debug log')
    })

    //指定targetPath
    program.on('option:targetPath', function () {
        process.env.CLI_TARGET_PATH = opts.targetPath
    })

    // 监听未知命令
    program.on('command:*', function (obj) {
        const allRegistryCommand = program.commands.map(cmd => cmd.name())
        console.log(colors.red('未知命令：' + obj[0]))
        console.log(colors.blue('可用命令', allRegistryCommand.join(',')));
    })

    // 输出错误信息
    const enhanceErrorMessages = require('./util/enhanceErrorMessages')

    enhanceErrorMessages('unknownOption', optionName => {
        return `Unknown option ${chalk.yellow(optionName)}.`
    })

    // 输出帮助文档
    if (!process.argv.slice(2).length) {
        program.outputHelp()
        console.log();
    }

    program.parse(process.argv)
}

// 脚手架启动阶段
async function prepare() {
    checkPkgVersion()
    checkRoot()
    checkUserHome()
    checkEnv()
    await checkGlobalUpdate()
}

// 检查版本号
function checkPkgVersion() {
    log.notice('cli', pkg.version);
}


// 检查root
function checkRoot() {
    // root 启动的目录无法操作，需要进行降级
    // sudo 启动 打印就是 0  正常就是 501
    const rootCheck = require('root-check')
    rootCheck()
    // console.log(process.getuid());
}

// 检查用户主目录
function checkUserHome() {
    if (!userHome || !pathExists(userHome)) {
        throw new Error(chalk.red('当前登陆用户主目录不存在！'))
    }
}


// 检查环境变量
function checkEnv() {
    const dotenv = require('dotenv')
    const dotenvPath = path.resolve(userHome, '.env')
    if (pathExists(dotenvPath)) {
        dotenv.config({
            path: dotenvPath
        })
    }
    createDefaultConfig() // 设置默认配置
    // log.verbose('环境变量', process.env.CLI_HOME_PATH)
}

// 设置默认的环境变量
function createDefaultConfig() {
    const cliConfig = {
        home: userHome,
    }
    if (process.env.CLI_HOME) {
        cliConfig['cliHome'] = path.join(userHome, process.env.CLI_HOME)
    } else {
        cliConfig['cliHome'] = path.join(userHome, constant.DEFAULT_CLI_HOME)
    }
    // console.log(cliConfig)
    process.env.CLI_HOME_PATH = cliConfig['cliHome']
}

// 检查全局变量是否需要更新
async function checkGlobalUpdate() {
    // 1. 获取当前版本号和模块名
    const currentVersion = pkg.version
    const npmName = pkg.name
    // 2. 调用npm API，获取所有版本号
    // 3. 提取所有版本号,比对哪些版本号大于当前版本号
    const { getNpmSemverVersion } = require('@cli-dev-zy/get-npm-info')
    const lastVersion = await getNpmSemverVersion(currentVersion, npmName)
    // 4. 获取最新版本号,提示用户更新到该版本
    if (lastVersion && semver.gt(lastVersion, currentVersion)) {
        log.warn('更新提示', colors.yellow(dedent`
        请手动更新 ${npmName}, 当前版本: ${currentVersion}, 最新版本: ${lastVersion}.
        更新命令: npm install -g ${npmName}
        ` ))
    }
}