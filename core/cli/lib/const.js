const LOWEST_NODE_VERSION = '12.0.0'  // node最低版本号
const DEFAULT_CLI_HOME = '.cli-dev-zy' // 脚手架默认目录

module.exports = {
    LOWEST_NODE_VERSION,
    DEFAULT_CLI_HOME
}