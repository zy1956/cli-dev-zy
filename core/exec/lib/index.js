'use strict';



// 1.targetPath -> modulePath
// 2. modulePath -> Package(npm模块)
// 3. Package.getRootFile(获取入口文件)
// 4. Package.update

// 封装 -> 复用
const path = require('path')
const Package = require('@cli-dev-zy/package');
const log = require('@cli-dev-zy/log')
const { exec: spawn } = require('@cli-dev-zy/utils')

const SETTINGS = {
    // init: '@cli-dev-zy/init'
    init: '@imooc-cli/init'
}

const CACHE_DIR = 'dependencies/'

async function exec(projectName, options, command) {
    let pkg = ''
    let targetPath = process.env.CLI_TARGET_PATH
    const homePath = process.env.CLI_HOME_PATH
    let storeDir = ''
    log.verbose('exec-targetPath', targetPath)
    log.verbose('exec-homePath', homePath)
    const cmdObj = arguments[arguments.length - 1];
    const cmdName = cmdObj.name();
    const packageName = SETTINGS[cmdName]
    const packageVersion = 'latest'

    if (!targetPath) {
        targetPath = path.resolve(homePath, CACHE_DIR); // 生成缓存路径
        storeDir = path.resolve(targetPath, 'node_modules')
        log.verbose('exec-targetPath', targetPath)
        log.verbose('exec-storeDir', storeDir)
        pkg = new Package({
            targetPath,
            storeDir,
            packageName,
            packageVersion
        })

        if (await pkg.exists()) {
            // 更新package
            await pkg.update()
        } else {
            // 安装package
            await pkg.install() // 这里是异步执行
        }
    } else {
        pkg = new Package({
            targetPath,
            packageName,
            packageVersion
        })
    }
    const rootFile = pkg.getRootFilePath()
    // console.log(rootFile);
    if (rootFile) {
        try {
            // 在当前进程中调用
            // require(rootFile).call(null, Array.from(arguments))
            // 在node子进程中调用
            let args = Array.from(arguments).splice(0, 2)
            const code = `require('${rootFile}').call(null,${JSON.stringify(args)})`
            // cp.spawn('cmd', ['/c', 'node', '-e', code])
            // console.log(code);
            const child = spawn('node', ['-e', code], {
                cwd: process.cwd(),
                stdio: 'inherit'
            })
            child.on('error', e => {
                log.error(e.message);
                process.exit(1);
            })
            child.on('exit', e => {
                log.verbose('命令执行失败' + e);
                process.exit(e);
            })
        } catch (e) {
            log.error(e.message)
        }
    } else {
        log.error('没有rootFile')

    }
}

module.exports = exec;