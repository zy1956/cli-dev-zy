'use strict';

const path = require('path')
const pkgDir = require('pkg-dir').sync
const pathExists = require('path-exists').sync
const npminstall = require('npminstall')
const fse = require('fs-extra')
const { isObject } = require('@cli-dev-zy/utils')
const { getDefaultRegistry, getNpmLatestVersion } = require('@cli-dev-zy/get-npm-info')
const formatPath = require('@cli-dev-zy/format-path')
class Package {
    constructor(options) {
        if (!options) {
            throw new Error('Package类的options参数不能为空')
        }
        if (!isObject(options)) {
            throw new Error('Package类的options参数必须为对象')
        }
        // package的路径
        this.targetPath = options.targetPath
        // package的存储路径
        this.storeDir = options.storeDir
        // package的name 
        this.packageName = options.packageName
        // package的version
        this.packageVersion = options.packageVersion
        // package的缓存目录前缀
        this.cacheFilePathPrefix = this.packageName.replace('/', '_')
    }
    async prepare() {
        if (this.storeDir && !pathExists(this.storeDir)) {
            // 创建所有文件目录
            fse.mkdirpSync(this.storeDir)
        }
        if (this.packageVersion === 'latest') {
            this.packageVersion = await getNpmLatestVersion(this.packageName)
        }
    }
    get cacheFilePath() {
        return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${this.packageVersion}@${this.packageName}`)
    }

    getSpecificCacheFilePath(packageVersion) {
        return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${packageVersion}@${this.packageName}`)
    }

    // 判断当前Package是否存在
    async exists() {
        if (this.storeDir) {
            await this.prepare()
            return pathExists(this.cacheFilePath)
        } else {
            return pathExists(this.targetPath)
        }
    }
    // 安装Package
    async install() {
        await this.prepare()
        return npminstall({
            root: this.targetPath, //模块路径
            storeDir: this.storeDir,
            registry: getDefaultRegistry(),
            pkgs: [
                {
                    name: this.packageName,
                    version: this.packageVersion
                }
            ]
        })
    }
    // 更新Package
    async update() {
        await this.prepare()
        // 1. 获取最新的npm模块版本号
        const latestPackageVersion = await getNpmLatestVersion(this.packageName)
        // 2. 查询最新版本号对应的路径是否存在
        const latestFilPath = this.getSpecificCacheFilePath(latestPackageVersion)
        // 3. 如果不存在，则直接安装最新版本
        if (!pathExists(latestFilPath)) {
            await npminstall({
                root: this.targetPath, //模块路径
                storeDir: this.storeDir,
                registry: getDefaultRegistry(),
                pkgs: [
                    {
                        name: this.packageName,
                        version: latestPackageVersion
                    }
                ]
            })
            this.packageVersion = latestPackageVersion
        } else {
            this.packageVersion = latestPackageVersion
        }

        return latestFilPath

    }

    // 获取入口文件的路径
    getRootFilePath() {
        function _getRootFile(targetPath) {
            // 1. 获取package.json所在目录 - pkg-dir
            const dir = pkgDir(targetPath)
            if (dir) {
                // 2. 读取package.json - require()
                const pkgFile = require(path.resolve(dir, 'package.json'))
                // console.log(pkgFile);
                // 3. 寻找main/lib - path
                if (pkgFile && pkgFile.main) {
                    // 4. 路径的兼容(mac/windows)
                    return formatPath(path.resolve(dir, pkgFile.main))
                }
            } else {
                return null
            }
        }
        //! 有缓存
        if (this.storeDir) {
            return _getRootFile(this.cacheFilePath)
            //! 没有缓存
        } else {
            return _getRootFile(this.targetPath)
        }
    }
}

module.exports = Package;

