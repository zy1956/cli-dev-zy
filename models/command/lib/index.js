'use strict';
const semver = require('semver')
const LOWEST_NODE_VERSION = '12.0.0'  // node最低版本号
const colors = require('colors/safe')
const log = require('@cli-dev-zy/log');
const { isObject } = require('@cli-dev-zy/utils')


class Command {
    constructor(argv) {
        log.verbose('command constructor', argv);
        if (!argv) {
            throw new Error('参数不能为空!')
        }
        if (!Array.isArray(argv)) {
            throw new Error('参数必须为数组!')
        }
        if (argv.length < 1) {
            throw new Error('参数列表为空!')
        }
        this._argv = argv
        let runner = new Promise((resolve, reject) => {
            let chain = Promise.resolve()
            chain = chain.then(() => this.checkNodeVersion())
            chain = chain.then(() => this.initArgs())
            chain = chain.then(() => this.init())
            chain = chain.then(() => this.exec())
            chain.catch(err => {
                log.error(err.message);
            })
        })
    }
    initArgs() {
        // const [projectName, options, command] = this._argv
        // console.log(projectName, options, command);
        const len = this._argv.length - 1
        this._cmd = this._argv[len]
        this._argv = this._argv.slice(0, len)
        // console.log(this._cmd, this._argv);
    }
    // 检查node版本
    checkNodeVersion() {
        // 1. 获取当前Node版本号
        // 2. 比对最低版本号
        const currentVersion = process.version
        const lowestVersion = LOWEST_NODE_VERSION
        if (!semver.gte(currentVersion, lowestVersion)) {
            throw new Error(colors.red(`cli 需要安装 v${lowestVersion} 以上版本的Node.js`))
        }
    }

    init() {
        throw new Error('init必须实现')
    }

    exec() {
        throw new Error('exec必须实现')

    }
}


module.exports = Command;
