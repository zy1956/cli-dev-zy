'use strict';
const path = require('path')

// 兼容windows和macOs路径 格式化
module.exports = function formatPath(p) {
    if (p && typeof p === 'string') {
        const sep = path.sep  // 分隔符
        if (sep === '/') {
            return p
        } else {
            return p.replace(/\\/g, '/')
        }
    }
    return p
}
